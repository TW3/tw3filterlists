@extends('_includes.base')
@section('body')

    <div class="welcome">
        <div class="wrapper">
            <section>
                <header>
                    <h1>{{ $siteName }}</h1>
                    <span>{{ $siteDescription }}</span>
                </header>
            </section>
        </div>
    </div>
	<br />
    <div class="left-side"><main>
        @markdown

## Filters.

Adblock Plus filters from https://github.com/collinbarrett/FilterLists

        @endmarkdown
    </main></div>

<hr />
<nav>
    <ul class="breadcrumb">
        <li class="breadcrumb-item"><button class="btn btn-action s-circle btn-sm bg-dark"></button></li>
        <li class="breadcrumb-item">
            <a href="@url('/pi-hole')" class="internal-link">Pi-hole</a>
        </li>
        <li class="breadcrumb-item">
            <a href="@url('/hosts')" class="internal-link">Hosts</a>
        </li>
        <li class="breadcrumb-item">
            <a href="@url('/dnsmasq')" class="internal-link">dnsmasq</a>
        </li>
        <li class="breadcrumb-item">
            <a href="@url('/umatrix')" class="internal-link">uMatrix | uBlock</a>
        </li>
        <li class="breadcrumb-item">
            <a href="@url('/')" class="internal-link">Adblock</a>
        </li>
    </ul>
</nav>
<hr />
<br />

@php
    $flistId = array('3', '28'); // Adblock Plus filters
@endphp

@stop