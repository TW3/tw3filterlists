<!DOCTYPE html>
<html lang="en-GB">

	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="@yield('pageDescription', $siteDescription)" />

		<title>{{$siteName}} @yield('pageTitle')</title>

		<link rel="shortcut icon" href="@url('assets/images/favicon.ico')" type="image/x-icon" sizes="16x16 32x32"/>
		<link rel="stylesheet" href="@url('assets/css/spectre.min.css')" />
		<link rel="stylesheet" href="@url('assets/css/spectre-icons.min.css')" />
		<link rel="stylesheet" href="@url('assets/css/all.css')" />
	</head>

	<body>

		<div class="container">
			<div class="columns">
				<div class="column col-12">
					<div class="p-centered">
						<a href="@url('/')" class="internal-link">
							<img src="@url('assets/images/tw3filterlists.svg')" class="img-responsive height-128" alt="tw3filterlists Logo" />
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="columns col-oneline" id="bodyYield">
				<div class="column col-8">@yield('body')
@php
	if (isset($jsonData)) {
		// Placeholder
	} else {
    	$jsonData = '../../FilterLists/data/FilterList.json';
    	$jsonDataCont = file_get_contents($jsonData);
		$jsonDataContents = preg_replace('/\x{FEFF}/u', '', $jsonDataCont); // Remove any BOM characters from the data
    	$jsonDecode = json_decode($jsonDataContents);
    	$jsonLicData = '../../FilterLists/data/License.json'; // License.json is UTF-8 BOM encoded..
    	$jsonLicDataCont = file_get_contents($jsonLicData);
		$jsonLicDataContents = preg_replace('/\x{FEFF}/u', '', $jsonLicDataCont);
    	$jsonLicDecode = json_decode($jsonLicDataContents);
		if ($jsonDecode != false) {
			if (function_exists('sort_by_name')){
				// Placeholder 
			} else {
				function sort_by_name( $a, $b ) { 
  					if(  $a->name ==  $b->name ){ return 0 ; } 
  						return ($a->name < $b->name) ? -1 : 1;
				}
			}
			usort($jsonDecode,'sort_by_name');
		}
	}
	if ($jsonDataContents != false) :
		if (function_exists('use_mirror')){
			// Placeholder 
		} else {
			function use_mirror( $z ) {
				//
				// Remove raw.githubusercontent.com links
				// Other possible mirrors could include
				// https://gitcdn.xyz/ | https://raw.githack.com/ | https://statically.io/
				//
  				if (strpos($z, 'raw.githubusercontent.com') !== false) {
    				// echo 'true';
					$y = str_replace('raw.githubusercontent.com', 'cdn.jsdelivr.net/gh', $z);
					$x = str_replace('/master/', '/', $y);
					return $x;
				} else {
					return $z;
				}
			}
		}
		echo '<table class="table" id="table-filters"><thead><tr>';
    	echo '<th>Name</th><th>Description</th><th>Url</th>';
    	echo '</tr></thead><tbody>';
		foreach ($jsonDecode as $jsonItem) :
			foreach ($flistId as $flistIdItem) :
				if ($jsonItem->syntaxId == $flistIdItem) :
    				echo '<tr>';
					if ($jsonItem->name != false) {
						if ($jsonItem->homeUrl != false) {
    						echo "<td><a href=\"$jsonItem->homeUrl\" class=\"external-link\">$jsonItem->name</a></td>";
						} else {
    						echo "<td>$jsonItem->name</td>";
						}
					}
					echo '<td>';
					if ($jsonItem->description != false) {
    					echo "$jsonItem->description";
					}
                	if ($jsonItem->forumUrl or $jsonItem->issuesUrl or $jsonItem->viewUrlMirror1 or $jsonItem->viewUrlMirror2 != false) {
                    	if ($jsonItem->description != false) {
                        	echo '<br /><hr />';
                    	}
			    		if ($jsonItem->forumUrl != false) {
    	    				echo "<form style=\"display: inline\" action=\"$jsonItem->forumUrl\" method=\"get\"><button class=\"btn btn-sm s-rounded bg-dark tooltip tooltip-bottom\" data-tooltip=\"$jsonItem->forumUrl\">Forum <i class=\"icon icon-link text-secondary\"></i></button></form>";
	    				}
    					if ($jsonItem->issuesUrl != false) {
							if ($jsonItem->id != '734') {
								if ($jsonItem->id != '735') {
    				    			echo "<form style=\"display: inline\" action=\"$jsonItem->issuesUrl\" method=\"get\"><button class=\"btn btn-sm s-rounded bg-dark tooltip tooltip-bottom\" data-tooltip=\"$jsonItem->issuesUrl\">Bug Report <i class=\"icon icon-link text-secondary\"></i></button></form>";
								}
							}
				    	}
	    				if ($jsonItem->viewUrlMirror1 != false) {
        					echo "<form style=\"display: inline\" action=\"$jsonItem->viewUrlMirror1\" method=\"get\"><button class=\"btn btn-sm s-rounded bg-dark tooltip tooltip-bottom\" data-tooltip=\"$jsonItem->viewUrlMirror1\">Mirror 1 <i class=\"icon icon-download text-secondary\"></i></button></form>";
				    	}
				    	if ($jsonItem->viewUrlMirror2 != false) {
    				    	echo "<form style=\"display: inline\" action=\"$jsonItem->viewUrlMirror2\" method=\"get\"><button class=\"btn btn-sm s-rounded bg-dark tooltip tooltip-bottom\" data-tooltip=\"$jsonItem->viewUrlMirror2\">Mirror 2 <i class=\"icon icon-download text-secondary\"></i></button></form>";
				    	}
				    	if ($jsonItem->licenseId != false) {
							if ($jsonLicDataContents != false) :
								foreach ($jsonLicDecode as $jsonLicItem) :
									if ($jsonLicItem->id == $jsonItem->licenseId ) {
										if ($jsonLicItem->descriptionUrl != false) {
											echo "<form style=\"display: inline\" action=\"$jsonLicItem->descriptionUrl\" method=\"get\"><button class=\"btn btn-sm s-rounded bg-dark tooltip tooltip-bottom\" data-tooltip=\"$jsonLicItem->descriptionUrl\">$jsonLicItem->name <i class=\"icon icon-link text-secondary\"></i></button></form>";
										}
									}
								endforeach;
							endif;
						}
                	}
					echo '</td><td>';
					if ($jsonItem->viewUrl != false) {
                    	echo "<form style=\"display: inline\" action=\"" . use_mirror("$jsonItem->viewUrl") . "\" method=\"get\"><button class=\"btn btn-action s-rounded bg-dark tooltip tooltip-bottom\" data-tooltip=\"" . use_mirror("$jsonItem->viewUrl") . "\"><i class=\"icon icon-download text-secondary\"></i></button></form>";
					}
                	echo '</td>';
    				echo '</tr>';
				endif;
			endforeach;
		endforeach;
		echo '</tbody></table>';
	endif;
@endphp

				</div>
				<div class="column col-4">
					<div class="right-side">@include('_includes.sidebar')</div>
				</div>
			</div>
		</div>

		<div class="container">
			<div class="columns">
				<div class="column col-12">
					<div id="footer"><span class="text-dark"><small>Copyleft <time>{{ $footerDate }}</time> TW3. All trademarks are copyright their various owners.</small></span></div>
				</div>
			</div>
		</div>

	</body>

</html>