<section>
    <h3>Code</h3>
    <aside>
        <span>Take a look under the hood :</span>
        <br /><br />

		<div class="container">
			<div class="columns">
				<div class="column col-6">
					<div class="card" id="card-support-gl">
						<div class="card-header">
							<div class="card-title h5">Hosted by</div>
						</div>
						<div class="card-image col-mx-auto"><a href="https://about.gitlab.com/" class="external-link"><img src="@url('assets/images/gitlab-logo-gray-rgb.svg')" class="img-responsive  width-128" alt="Gitlab" /></a></div>
						<div class="card-footer"><form style="display: inline" action="https://gitlab.com/TW3/tw3filterlists" method="get"><button class="btn btn-primary">Fork the code</button></form></div>
					</div>
				</div>
			</div>
		</div>

        <br /><br />
        <span>tw3filterlists is licensed under the terms of the <a href="https://gitlab.com/TW3/tw3filterlists/blob/master/LICENSE.txt" class="external-link">MIT License</a>.</span>
        <br />
        <span><!-- Some content could maybe go here: --></span>
        <br /><hr /><br />

        <div class="container">
            <div class="columns">
                <div class="column col-3">
                    <div id="show-for-js-users" style="visibility: hidden;">
                        <h3>Search</h3>
		                <div class="form-group">
                            <form id="search-form">
                                <input class="form-input input-lg bg-dark text-secondary" type="text" id="name-search-input" onkeyup="searchBox()" placeholder="type in a name.." title="Please type a filter name">
                                <br />
                                <button class="btn s-rounded bg-dark input-group-btn" onclick="resetSearchBox()"><i class="icon icon-refresh text-secondary"></i></button>
                            </form>
                        </div>
                    </div>
	            </div>
            </div>
        </div>

<script>
    function searchBox() {
        var input, filter, table, tr, td, i, txtValue;
        input = document.getElementById("name-search-input");
        filter = input.value.toUpperCase();
        table = document.getElementById("table-filters");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    function resetSearchBox() {
        document.getElementById("search-form").reset();
        searchBox();
    }
    document.getElementById("show-for-js-users").style.visibility = "visible";
</script>

        <br />
        <span class="text-dark"><!-- Some content could maybe go here: --></span>
        <br />
    </aside>
</section>